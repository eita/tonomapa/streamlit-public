import os
import requests
from requests.structures import CaseInsensitiveDict

from dotenv import load_dotenv

class ApiData():
    def __init__(self):

        token = self.get_token()
        self.access_token = token.get('access')
        self.refresh_token = token.get('refresh')

        # set headers request api
        headers = CaseInsensitiveDict()
        headers["Accept"] = "application/json"
        headers["Authorization"] = f"Bearer {self.access_token}"
        self.headers = headers

    def get_token(self):
        # load .env
        load_dotenv()
        env = os.environ

        # access token tonomapa
        self.api_host = env.get('API_HOST')
        self.api_host_endpoint = f"{self.api_host}/api"
        url_auth = f"{self.api_host}/api-authentication/token/"
        payload = {'username': env.get('API_USER'),
                   'password': env.get('API_PASSWD')}
        req = requests.post(url_auth, data=payload)
        res = req.json()
        return res

    def get_endpoints(self, print_endpoints=True):
        url = f"{self.api_host_endpoint}/endpoints/"
        req = requests.get(url, headers=self.headers)
        res = req.json()
        if print_endpoints:
            self.print_endpoints(res)
        return res

    def print_endpoints(self, endpoints):
        print()
        print("#######################")
        print("Data API endpoint list")
        print("#######################")
        print()
        for ep in endpoints:
            print(f"{ep['model_name']}: {ep['verbose_name']}")
        print()

    def get_endpoint(self, endpoint):
        url = f"{self.api_host_endpoint}/{endpoint}/"
        req = requests.get(url, headers=self.headers)
        res = req.json()
        return res
