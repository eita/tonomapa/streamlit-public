#!/bin/bash

ssh -T $CI_DEPLOY_USER@$CI_DEPLOY_HOST -p $CI_DEPLOY_PORT <<EOL
cd $CI_DEPLOY_PROJECT_ROOT
git pull origin $BRANCH_PRODUCTION
source venv/bin/activate
pip install -r requirements.txt
EOL

echo "Deploy executado!"
