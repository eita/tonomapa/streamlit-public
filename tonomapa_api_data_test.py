import streamlit as st
import tonomapa_api_data

api_data = tonomapa_api_data.ApiData()

# retornar lista de endpoints
endpoints = api_data.get_endpoints(print_endpoints=True)

st.write("""
# Dashboard
*Olá!*
""")

# Example:
territorio_tipos_comunidade = api_data.get_endpoint(
    'territorio_tipos_comunidade')

# Enable pdb to DEBUG
import pdb; pdb.set_trace()

